package com.db;

/**
 * Created by Student on 22.04.2015.
 */

import com.db.services.BankFileDataLoaderService;
import com.db.services.BankService;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class BankServer {

    BankServer() {

    }
    void listenClient(ObjectInputStream in, ObjectOutputStream out) throws IOException{
        String message = null;
        do {
            try {
                message = (String) in.readObject();
                System.out.println("client>" + message);

                switch (message) {
                    case "bye":
                        sendMessage(out, "bye");
                        break;
                    case "bankReport":
                        BankFileDataLoaderService bankService = new BankFileDataLoaderService("resources/feed.txt");

                        out.writeObject(bankService.readBank());
                        sendMessage(out, "bankreport was sent");
                        break;
                    default:
                        sendMessage(out, "???" + message);
                }

            } catch (ClassNotFoundException classnot) {
                System.err.println("Data received in unknown format");
            }
        } while (!"bye".equals(message));
    }

    void startServer() {
        try (ServerSocket providerSocket = new ServerSocket(2004,10)) {
            System.out.println("Server started on port 2004");
            while (true) {
                System.out.println("Waiting for connection");
                Socket connection = providerSocket.accept();
                System.out.println("Connection received from "
                        + connection.getInetAddress().getHostName());
                new Thread(() -> {
                    try (Socket session = connection;
                         ObjectOutputStream out = new ObjectOutputStream(session.getOutputStream());
                             ObjectInputStream in = new ObjectInputStream(session.getInputStream()) )
                    {
                        sendMessage(out, "Connection successful");
                        out.flush();
                        listenClient(in,out);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    void sendMessage(ObjectOutputStream out, final String msg) {
        try {
            out.writeObject(msg);
            out.flush();
            System.out.println("server>" + msg);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static void main(final String args[]) {
        BankServer server = new BankServer();
        while (true) {
            server.startServer();
        }
    }
}
