package com.db.domain;

import com.db.Exeptions.OverDraftLimitExceededException;

import java.io.Serializable;

/**
 * Created by Student on 17.04.2015.
 */
public class CheckingAccount extends AbstractAccount{
    private double overdraft;

    public double getOverdraft() {
        return overdraft;
    }

    @Override
    public double getBalance() {

        return amount - overdraft;
    }

    @Override
    public void withdraw(double x) throws OverDraftLimitExceededException{
        if (amount < x + overdraft) throw new  OverDraftLimitExceededException("Couldn't withdraw "
                + x + " only " + amount + " is available ");
        amount -= x;
    }

    @Override
    public double maximumAmountToWithdraw() {
        return amount;
    }

    public CheckingAccount(double x, double ovr) {
        super(x + ovr);
        if (x < 0) throw new IllegalArgumentException();
        overdraft = ovr;
    }

    @Override
    public String toString(){
        return "credit account. balance is " + getBalance() + " max overdraft is " + getOverdraft();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CheckingAccount)) return false;

        CheckingAccount that = (CheckingAccount) o;

        if (Double.compare(that.amount, amount) != 0) return false;
        return Double.compare(that.overdraft, overdraft) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(amount);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(overdraft);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


}
