package com.db.domain;

import com.db.Exeptions.NotEnoughFundsException;

import java.io.Serializable;

/**
 * Created by Student on 17.04.2015.
 */
public interface Account extends Serializable {

    double getBalance();

    void deposit(double x);

    void withdraw(double x) throws NotEnoughFundsException;

    double maximumAmountToWithdraw();
}
