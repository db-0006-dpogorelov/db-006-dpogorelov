package com.db.domain;


import com.db.Exeptions.BankException;
import com.db.Listeners.ClientRegistrationListener;
import com.db.Listeners.EmailNotificationListener;
import com.db.Listeners.PrintClientListener;
import com.db.services.EmailService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Student on 16.04.2015.
 */

public class Bank implements Serializable{
    private List<Client> clients = new ArrayList<>();
    transient private List<ClientRegistrationListener> listeners = new LinkedList<>();
    transient  public EmailService emailService;

    public List<ClientRegistrationListener> getListeners() {
        return listeners;
    }



    public List<Client> getClientList(){
        return clients;
    }

    public Bank(){
        emailService = new EmailService();
        emailService.start();
        listeners.add (new PrintClientListener());
        listeners.add (new EmailNotificationListener(emailService));
        listeners.add (new ClientRegistrationListener() {
            @Override
            public void onClientAdded(Client client) {
                // todo: some action here.
            }
        });
    }

    public Bank(List<Client> clients, List<ClientRegistrationListener> listeners){
        this.clients = clients;
        this.listeners = listeners;

    }
}
