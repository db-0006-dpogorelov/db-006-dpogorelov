package com.db.domain;


import com.db.Exeptions.ClientProfileException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 16.04.2015.
 */
public class Client implements Serializable {
    private List<Account> accounts = new ArrayList<>();
    private String name;
    private Gender gender;
    private String email;

    public String getName() {
        return name;
    }

    public String getSalutation() {
        return gender.getSalutation() + " " + name;
    }

    public List<Account> getAccountList(){
        return accounts;
    }

    public void setName(String name) throws ClientProfileException{
        if (name == null) throw new ClientProfileException("Invalid name");
        this.name = name;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setEmail(String email) throws  ClientProfileException{
        if (email == null) throw  new ClientProfileException("Invalid email");
        this.email = email;
    }

    public void addAccount(Account account){
        accounts.add(account);
    }

    public Client(Gender gender, String name, String email, Account... accounts) throws ClientProfileException {
        if (name == null) throw new ClientProfileException("Invalid name");
        if (email == null) throw new ClientProfileException("Invalid email");
        this.name = name;
        this.gender = gender;
        this.email = email;
        for (Account account: accounts) {
            this.accounts.add(account);
        }
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return ! (name != null ? !name.equals(client.name) : client.name != null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        return result;
    }

    @Override
    public String toString() {
        StringBuffer clientdetails = new StringBuffer("");
        clientdetails.append("Client{ ");
        clientdetails.append(getSalutation());
        clientdetails.append(", email = ");
        clientdetails.append(email);
        clientdetails.append(", accounts: \n");

        for (Account account: accounts){
            clientdetails.append("   ");
            clientdetails.append(account.toString());
            clientdetails.append("\n");
        }
        clientdetails.append("\n}");
        return clientdetails.toString();
    }
}

