package com.db.domain;

import java.io.Serializable;

/**
 * Created by Pogorelych on 21.04.2015.
 */
abstract public class AbstractAccount implements Account {

    protected double amount = 0;

    public AbstractAccount(double x) throws IllegalArgumentException {
        if (x < 0) throw new IllegalArgumentException();
        amount = x;
    }

    @Override
    public void deposit(double x) throws IllegalArgumentException {
        if (x < 0) throw new IllegalArgumentException();
        amount += x;

    }
}
