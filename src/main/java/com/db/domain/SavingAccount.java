package com.db.domain;

import com.db.Exeptions.NotEnoughFundsException;

/**
 * Created by Student on 16.04.2015.
 */
public class SavingAccount extends AbstractAccount {

    public SavingAccount(double x) {
        super(x);
    }

    @Override
    public double getBalance(){
        return amount;
    }

    @Override
    public void withdraw(double x) throws NotEnoughFundsException {
        if (x > amount) throw new NotEnoughFundsException("Couldn't withdraw "
                + x + " only " + amount + " is available ");
        amount -= x;
    }

    @Override
    public double maximumAmountToWithdraw() {
        return amount;
    }

    @Override
    public String toString() {
        return "saving account. balance is: " + getBalance();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SavingAccount)) return false;

        SavingAccount that = (SavingAccount) o;

        return Double.compare(that.amount, amount) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(amount);
        return (int) (temp ^ (temp >>> 32));
    }
}
