package com.db.services;

import java.util.StringTokenizer;

/**
 * Created by Student on 23.04.2015.
 */
public class Email {
    private String address;
    private String body;

    public String getAddress() {
        return address;
    }

    public Email(String address, String body) {
        this.address = address;
        this.body = body;
    }
}
