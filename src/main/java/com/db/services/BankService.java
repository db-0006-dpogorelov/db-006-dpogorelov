package com.db.services;

import com.db.Exeptions.BankException;
import com.db.Exeptions.ClientProfileException;
import com.db.Listeners.ClientRegistrationListener;
import com.db.domain.Bank;
import com.db.domain.Client;
import com.db.Exeptions.DuplicateClientException;
import com.db.domain.Gender;

import java.io.*;
import java.util.List;

/**
 * Created by Student on 17.04.2015.
 */
public class BankService {
    private Bank bank;

    public BankService(Bank bank) {
        this.bank = bank;
    }

    public List<Client> getClientList() {
        return bank.getClientList();
    }

    public List<ClientRegistrationListener> getListeners(){
        return bank.getListeners();
    }

    public void addListener(ClientRegistrationListener newListener){
        getListeners().add(newListener);
    }

    public void removeListener(int i){
        //todo: throw exception
        getListeners().remove(i);
    }

    public String getReport() {
        StringBuffer report = new StringBuffer();
        report.append("Total ");
        report.append(getClientList().size());
        report.append(" clients in bank");
        for (Client client : getClientList()) {
                report.append(client);
        }
        return report.toString();
    }

    public Client getClient(int i) throws BankException {
        //todo: throw exception
        return bank.getClientList().get(i);
    }

    public void removeClient(int i) throws BankException {
        //todo: throw exception
        bank.getClientList().remove(i);
    }

    public Client addClient(Client newClient) throws ClientProfileException {
        if (newClient == null) throw new ClientProfileException("Client can't be null");
        for (Client client : bank.getClientList()) {
            if (newClient.equals(client)) throw new DuplicateClientException(newClient.getName());
        }
        getClientList().add(newClient);
        for (ClientRegistrationListener listener : bank.getListeners()) {
                listener.onClientAdded(newClient);
        }
        return newClient;
    }

    public Client addClient(String name, String email) throws ClientProfileException {
        return addClient(Gender.DEFAULT, name, email);
    }

    public Client addClient(Gender gender, String name, String email) throws ClientProfileException {
        Client  newClient = new Client(gender,name,email);
        addClient(newClient);
        return  newClient;
    }

    public void saveBank() throws IOException {
        try (ObjectOutputStream  out = new ObjectOutputStream(new FileOutputStream("resources/bank.dat"))) {
            out.writeObject(bank);
        } catch (IOException e) {
            throw e;
        }

    }




}
