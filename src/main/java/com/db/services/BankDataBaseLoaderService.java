package com.db.services;

import com.db.Exeptions.BankException;
import com.db.domain.Account;
import com.db.domain.CheckingAccount;
import com.db.domain.Client;
import com.db.domain.SavingAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 24.04.2015.
 */
public class BankDataBaseLoaderService implements BankDataLoaderService{
    private String dataBaseURL;

    public BankDataBaseLoaderService(String dataBaseURL) {
        this.dataBaseURL = dataBaseURL;
    }

    public static void main(String[] args) {
        BankDataLoaderService service = new BankDataBaseLoaderService( "jdbc:derby://localhost/bankapp;create=true");
        try {
            service.loadClients();
        } catch (BankException e) {
            e.printStackTrace();
        }

    }


    @Override
    public List<Client> loadClients() throws BankException {
        List<Client> clients = new ArrayList<>();
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            try (Connection conn
                         = DriverManager.getConnection(dataBaseURL)) {


                PreparedStatement selStm =
                        conn.prepareStatement("SELECT name,gender,email,accounttype,balance,overdraft FROM CLIENTS");
                ResultSet resultSet = selStm.executeQuery();

                while (resultSet.next()) {
                    String name;
                    String gender;
                    String accounttype;
                    String email;
                    double balance;
                    double overdraft;
                    Account account;

                    name = resultSet.getString("name");
                    gender = resultSet.getString("gender");
                    accounttype = resultSet.getString("accounttype");
                    email = resultSet.getString("email");
                    balance = resultSet.getDouble("balance");
                    overdraft = resultSet.getDouble("overdraft");

                    switch (accounttype) {
                        case "c":
                            account = new CheckingAccount(balance, overdraft);
                            break;
                        case "s":
                            account = new SavingAccount(balance);
                            break;
                    }

                }

                conn.commit();

            } catch (SQLException e) {
                throw e;
            }

        } catch (ClassNotFoundException e) {
            throw new BankException(e);
        } catch (SQLException e) {
            throw new BankException(e);
        }
        return clients;
    }

}



