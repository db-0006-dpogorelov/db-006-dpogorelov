package com.db.services;

import com.db.Exeptions.BankException;
import com.db.Exeptions.ClientProfileException;
import com.db.domain.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Student on 21.04.2015.
 */
public class BankFileDataLoaderService implements BankDataLoaderService {
    private String feedFileName;

    public BankFileDataLoaderService(String fileName) {
        this.feedFileName = fileName;
    }

    @Override
    public List<Client> loadClients() throws BankException {
        List<Client> clients = new ArrayList<>();
        try( BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(feedFileName)))) {
            String line;
            line = in.readLine();
            while (line != null) {
                try{
                    clients.add(parseString(line));
                } catch (ClientProfileException  e){
                    throw new  BankException(e);
                }
                line = in.readLine();
            }
        } catch (IOException e){
            throw new BankException("Couldn't load feed file",e);
        }
        return clients;
    }

    public Client parseString(String line) throws ClientProfileException {
        StringTokenizer tokenizer = new StringTokenizer(line,";");
        String email = null;
        String name = null;
        Gender gender = Gender.DEFAULT;
        Account account = null;
        String acctype = null;
        double balance = 0;
        double overdraft = 0;
        System.out.println(line);
        while (tokenizer.hasMoreTokens()){
            StringTokenizer fieldTok = new StringTokenizer(tokenizer.nextToken(),"=");
            switch (fieldTok.nextToken()){
                case "name":
                    name = fieldTok.nextToken();
                    break;
                case "email":
                    email = fieldTok.nextToken();
                    break;
                case "gender":
                    switch (fieldTok.nextToken()){
                        case "m":
                            gender=Gender.MALE;
                            break;
                        case "f":
                            gender = Gender.FEMALE;
                    }
                    break;
                case "accounttype":
                    acctype = fieldTok.nextToken();
                    break;
                case "balance":
                    balance = Double.parseDouble(fieldTok.nextToken());
                    break;
                case "overdraft":
                    overdraft = Double.parseDouble(fieldTok.nextToken());
                    break;
            }
        }
        if (acctype != null) {
            switch (acctype) {
                case "c":
                    account = new CheckingAccount(balance, overdraft);
                    break;
                case "s":
                    account = new SavingAccount(balance);
                    break;
                }
        }
        Client client = null;
            if (account != null) {
                client = new Client(gender, name, email, account);
            } else {
                client = new Client(gender, name, email);
            }
        return  client;
    }

    public Bank readBank() throws IOException, ClassNotFoundException {
        Bank bank;
        try (
                ObjectInputStream in = new ObjectInputStream(new FileInputStream("resources/bank.dat")))
        {
            bank = (Bank) in.readObject();
        } catch (IOException e) {
            throw e;
        }
        return bank;
    }
}
