package com.db.services;

import com.db.domain.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 23.04.2015.
 */
public class EmailService {
    private EmailQueue queue = new EmailQueue();
    transient  private Thread servicer;
    private boolean readyToClose = false;

    public void start() {
        servicer = new Thread() {
            @Override
            public void run() {
                while (true) {
                    synchronized (queue){
                        if (queue.isEmpty() && readyToClose) {
                            System.out.println("Mail server shooting down");
                            return;
                        }
                    }
                    try {
                        Thread.sleep(500);
                        } catch (InterruptedException e) {
                        if (queue.isEmpty()){
                            System.out.println("Mail server shooting down");
                            return;
                        }
                    }
                    synchronized (queue) {
                        if (!queue.isEmpty()) {
//                         some actions for sending email
                            System.out.println("Notification email was sent to " + queue.pop().getAddress());
                        }
                    }
                }
            }

        };
        servicer.start();
        System.out.println("Mail server was started");
    }

    public void close(){
        synchronized (queue){
            readyToClose = true;
        }
        servicer.interrupt();

    }

    public void SendNotifiactionEmail(Client client){
        synchronized (queue) {
            queue.add(new Email(client.getEmail(), client.toString()));
        }
    }
}
