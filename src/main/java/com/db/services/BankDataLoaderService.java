package com.db.services;

import com.db.Exeptions.BankException;
import com.db.domain.Client;

import java.util.List;

/**
 * Created by Student on 24.04.2015.
 */
interface BankDataLoaderService {
    List<Client> loadClients() throws BankException;
}
