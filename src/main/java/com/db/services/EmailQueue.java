package com.db.services;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Student on 23.04.2015.
 */
public class EmailQueue {
    private List<Email> queue = new LinkedList<Email>();


    public void add(Email email) {
            queue.add(email);
    }

    public Email pop(){

            return queue.remove(0);
    }

    public boolean isEmpty(){
            return queue.isEmpty();
    }
}
