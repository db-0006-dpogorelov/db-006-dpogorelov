package com.db.Exeptions;

/**
 * Created by Student on 20.04.2015.
 */
public class DuplicateClientException extends ClientProfileException {

    public DuplicateClientException() {
    }

    public DuplicateClientException(String name) {
        super("Client  with name " + name + " already exists");
    }

    public DuplicateClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateClientException(Throwable cause) {
        super(cause);
    }

    public DuplicateClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
