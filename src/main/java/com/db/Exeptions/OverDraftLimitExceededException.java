package com.db.Exeptions;

/**
 * Created by Student on 20.04.2015.
 */
public class OverDraftLimitExceededException extends NotEnoughFundsException {
    public OverDraftLimitExceededException() {
    }

    public OverDraftLimitExceededException(String message) {
        super(message);
    }

    public OverDraftLimitExceededException(String message, Throwable cause) {
        super(message, cause);
    }

    public OverDraftLimitExceededException(Throwable cause) {
        super(cause);
    }

    public OverDraftLimitExceededException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
