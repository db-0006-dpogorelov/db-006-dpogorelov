package com.db.Exeptions;

/**
 * Created by Student on 20.04.2015.
 */
public class ClientProfileException extends BankException {
    public ClientProfileException() {
    }

    public ClientProfileException(String message) {
        super(message);
    }

    public ClientProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientProfileException(Throwable cause) {
        super(cause);
    }

    public ClientProfileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
