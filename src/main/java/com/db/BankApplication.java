package com.db;

import com.db.Exeptions.BankException;
import com.db.Exeptions.ClientProfileException;
import com.db.domain.*;
import com.db.services.BankFileDataLoaderService;
import com.db.services.BankService;

import java.io.IOException;
import java.util.List;

/**
 * Created by Student on 16.04.2015.
 */
public class BankApplication {
    public static void modifyBank(BankService bankService) {
//           bankService.getClientList()[1].getAccountList()[0].deposit(100);
//            try{
//                bankService.getClientList()[1].getAccountList()[1].withdraw(100);
//            } catch (NotEnoughFundsException e) {
//                System.out.println("Can not withdraw");
//            }
    }

    public static void main(String[] args) throws IOException{
        switch (args[0]) {
            case "-server":
                BankServer bankServer = new BankServer();
                bankServer.startServer();
                break;
            case "-client":
                BankClient bankClient = new BankClient();
                bankClient.run();
                break;
            case "-generatefeed":
                BankFileDataLoaderService dataLoaderService = new BankFileDataLoaderService("resources/feed.txt");
                Bank bank = new Bank();
                BankService bankService = new BankService(bank);
                List<Client> clients = null;


                try {
                    clients = dataLoaderService.loadClients();
                } catch (BankException e) {
                    e.printStackTrace();
                }


                for (Client client: clients){
                    try {
                        bankService.addClient(client);
                    } catch (ClientProfileException e) {
                        System.out.println("Couldn't add client " + client);
                        System.out.println("because of: " + e.getMessage());
                    }
                }

                bankService.saveBank();

                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                }

                System.out.println("Trying to shut down mail service");
                bank.emailService.close();
                System.out.println(bankService.getReport());
        }

    }
}
