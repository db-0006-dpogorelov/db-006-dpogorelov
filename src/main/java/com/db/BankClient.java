package com.db;

import com.db.domain.Bank;
import com.db.services.BankService;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class BankClient {

    Socket requestSocket;
    ObjectOutputStream out;
    ObjectInputStream in;
    String message;
    BufferedReader reader;

    BankClient() {
    }

    void run() {
        try {
            // 1. creating a socket to connect to the server
            requestSocket = new Socket("localhost", 2004);
            System.out.println("Connected to localhost in port 2004");
            // 2. get Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());
            reader = new BufferedReader(new InputStreamReader(System.in));
            // 3: Communicating with the server
            do {
                try {
                    message = (String) in.readObject();
                    System.out.println("server>" + message);
                    //sendMessage("Hi my server");
                    message = reader.readLine();
                    sendMessage(message);
                    if ("bankReport".equals(message)) {
                        Bank bank = (Bank) in.readObject();
                        BankService bankService = new BankService(bank);
                        System.out.println(bankService.getReport());
                    }
                } catch (ClassNotFoundException classNot) {
                    System.err.println("data received in unknown format");
                }
            } while (!message.equals("bye"));
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            // 4: Closing connection
            try {
                in.close();
                out.close();
                reader.close();
                requestSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    void sendMessage(final String msg) {
        try {
            out.writeObject(msg);
            out.flush();
            System.out.println("client>" + msg);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static void main(final String args[]) {
        BankClient client = new BankClient();
        client.run();
    }

}
