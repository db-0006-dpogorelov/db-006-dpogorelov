package com.db.Listeners;

import com.db.domain.Client;
import com.db.services.EmailService;

import java.io.Serializable;

/**
 * Created by Pogorelych on 18.04.2015.
 */
public class EmailNotificationListener implements ClientRegistrationListener {
    private EmailService emailService;

    public EmailNotificationListener(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public void onClientAdded(Client client) {

            emailService.SendNotifiactionEmail(client);


    }
}
