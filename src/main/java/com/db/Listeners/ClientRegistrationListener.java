package com.db.Listeners;

import com.db.domain.Client;

import java.io.Serializable;

/**
 * Created by Student on 17.04.2015.
 */
public interface ClientRegistrationListener extends Serializable {
    void onClientAdded(Client client);
}
