package com.db.Listeners;

import com.db.domain.Client;

/**
 * Created by Pogorelych on 18.04.2015.
 */
public class PrintClientListener implements ClientRegistrationListener{
    @Override
    public void onClientAdded(Client client) {
        System.out.println(client.getSalutation() +  " was added to bank database");

    }
}